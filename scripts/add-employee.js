(function () {
  'use strict';
  window.addEventListener('load', function () {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();



$(document).ready(function () {
  var max__email_fields = 10; //maximum input boxes allowed
  var email_block = $("#email-block");
  var add_email = $("#add-email");

  var max_phone_fields = 10;
  var phone_block = $("#phone-block");
  var add_phone = $("#add-phone");

  var x = 1; // for email
  var y = 1; // for phone

  $(add_email).click(function (e) {
    e.preventDefault();
    if (x < max__email_fields) {
      x++; 
      $(email_block ).append('<div><label class="input-label" for="email-input">Email '+ x +'</label><div style="display:-webkit-box"><input type="text" class="form-control" placeholder=""/><a href="#" class="remove-field"><i class="fas fa-times fa-xs"></i></a></div></div>');
    }
  });
  $(email_block).on("click", ".remove-field", function (e) {
    e.preventDefault(); 
    $(this).parent('div').parent('div').remove(); 
    x--;
  })

  $(add_phone).click(function(e) {
    e.preventDefault();
    if (y < max_phone_fields) {
      y++;
      $(phone_block).append('<div><label class="input-label" for="phone-input">Phone ' + y +'</label><div style="display:-webkit-box"><input type="text" class="form-control" id="phone-input" placeholder=""/><a href="#" class="remove-field"><i class="fas fa-times fa-xs"></i></a></div></div>');
    }
  });
  $(phone_block).on("click", ".remove-field", function (e) {
    e.preventDefault();
    $(this).parent('div').parent('div').remove(); 
    y--;
  })


  /*$( "#myForm" ).submit(function( event ) {
    event.preventDefault();
    var name_input = $('input#name-input').val();
    var id_code = $('input#name-input').val()
    console.log($('input#name-input').val());
    console.log( $( "#myForm" ).val());
  });*/
  
  if($('#percentInput').val().lenght < 3) {
    console.log($('#percentInput').val());
  }

});


